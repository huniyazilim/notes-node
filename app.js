"use strict";

const fs = require('fs');
const _ = require('lodash');
const yargs = require('yargs');

const argv = yargs.argv;
const notes = require('./notes.js');

const command = argv._[0]; // yargs marifeti ile komutu al.

let message = "";

if (!command) return;

switch (command) {
    case "add":
        const newNote = notes.addNote(argv.title, argv.body);
        message = newNote ? "Not başarıyla eklendi" : "Mükerrer başlık. Çıkıyorum!";
        console.log(message);
        break;
    case "remove":
        const isNodeRemoved = notes.removeNote(argv.title);
        message = isNodeRemoved ? `${argv.title} başlıklı not silindi.` : `${argv.title} başlıklı bir not mevcut değil!`;
        console.log(message);
        break;   
    case "read":
        const note = notes.getNote(argv.title);
        message = note ? `${argv.title} - ${note.body} ` : `${argv.title} başlıklı bir not mevcut değil!`;
        console.log(message);
        break;    
    case "list":
        const notesList = notes.getList();
        if (notesList.length > 0 ) {
            notesList.forEach(function(note) {
                console.log(`${note.title} - ${note.body}`);
            });
        } else {
            console.log("Hiç notunuz yok!")
        }
        break;                          
    default:
        console.log("Bilinmeyen komut!");
        break;
}