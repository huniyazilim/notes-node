"use strict";

const fs = require("fs");

const originalNote = {
    title : "Bir başlık!",
    body : "Burada metin olacak"
}

const originalNoteString = JSON.stringify(originalNote);

fs.writeFile("notes.json", originalNoteString, function(err) {
    if(err){
        console.log(err);
        return;
    }

    fs.readFile("notes.json", function(err, data){
        if (err){
            console.log(err);
            return;
        }

        const note = JSON.parse(data);

        console.log(typeof note);
        console.log(note.title);
    })    

});