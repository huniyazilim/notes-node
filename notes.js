"use strict";

const fs = require('fs');

const NOTES_FILE = "notes-data.json";

const titleExists = function titleExists(title, notes) {
    const duplicates = notes.filter(note => note.title === title);
    return duplicates.length > 0;
}

const fetchAllNotes = function fetchAllNotes(fileName) {
    let notes = [];
    if (fs.existsSync(fileName)){
        notes = JSON.parse(fs.readFileSync(fileName));
    }
    return notes;
}

const writeAllNotes = function writeAllNotes(notes, fileName) {
    fs.writeFileSync(fileName,JSON.stringify(notes,null, 2));
}

const addNote = (title, body) => {
    let notes = fetchAllNotes(NOTES_FILE);
    const newNote = { title, body };
    if (!titleExists(title, notes)){
        notes.push(newNote);
        writeAllNotes(notes, NOTES_FILE);
        return newNote;
    }
}

const removeNote = (title) => {
    const notes = fetchAllNotes(NOTES_FILE);
    if (titleExists(title, notes)){
        const newNotes = notes.filter(note => note.title != title);
        writeAllNotes(newNotes, NOTES_FILE);    
        return true;
    } else {      
        return false;
    } 
}

const getNote = (title) => {
    const notes = fetchAllNotes(NOTES_FILE);
    return notes.filter(note => note.title === title)[0];
}

const getList = () => {
    return fetchAllNotes(NOTES_FILE);
}

module.exports = {
    addNote,
    removeNote,
    getNote,
    getList
}
